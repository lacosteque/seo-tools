$(function () {
    
        
    $.summary = function () {
    $("html, body").animate({ scrollTop: $("#result").offset().top }, 1000);

if ($("#top").length) {
    $("#summary").prepend('<li class="list-group-item"><span class="fw-bolder">Top:</span> ' + $("#top").val() + "</li>");
}

if ($("#region").length) {
    $("#summary").prepend('<li class="list-group-item"><span class="fw-bolder">Region:</span> ' + $("#region").text() + "</li>");
    }

    if ($("#serp").length) {
        $("#summary").prepend('<li class="list-group-item"><span class="fw-bolder">SERP:</span> ' + $("#serp").val() + "</li>");
    }

    if ($("#website").length) {
        $("#summary").prepend('<li class="list-group-item"><span class="fw-bolder">Website:</span> ' + $("#website").val() + "</li>");
    }
};


    $.progressbar = function () {
        var total_queries = $("textarea#queries").val().split("\n").length;
        var total_time = total_queries * 1.75 * 1000;

        var bar = new ProgressBar.Line(progress, {
            strokeWidth: 4,
            easing: "easeInOut",
            duration: total_time,
            color: "#FFEA82",
            trailColor: "#eee",
            trailWidth: 1,
            svgStyle: { width: "100%", height: "100%" },
            text: {
                style: {
                    color: "#999",
                    position: "absolute",
                    right: "0",
                    top: "30px",
                    padding: 0,
                    margin: 0,
                    transform: null,
                },
                autoStyleContainer: false,
            },
            from: { color: "#FFEA82" },
            to: { color: "#ED6A5A" },
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) + " %");
            },
        });

        bar.animate(1.0);
    };

    
    $.column_num = function () {
$('#table tbody tr').each(function(i) {
    var number = i + 1;
    $(this).find('td:first').text(number);
});
}

    $("#tool-form").submit(function (e) {
        e.preventDefault();

        var form = $(this);

        var website = $("#website").val();
        var top = $("#top").val();
        var region = $("#region").attr("data-id");
        var serp = $("#serp").val();
        var queries = $("#queries").val();
        var data = {
            "website": [{ "value": website }],
            "top": [{ "value": top }],
            "region": [{ "value": region }],
            "serp": [{ "value": serp }],
            "queries": [{ "value": queries }],
        };

        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: JSON.stringify(data),
            dataType: "html",

            beforeSend: function (xhr, settings) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
                $.progressbar();
                form.find("button").prop("disabled", true);
            },
        })
            .done(function (result) {
                $("#result").html(result);

                if ($("#error").length) {
                    $("#progress").empty();
                    console.log(result);
                } else {
                    $.summary();

                    $("#progress").empty();
                    $("#table").bootstrapTable();
                    $.column_num();
                }

                form.find("button").prop("disabled", false);
            })
            .fail(function (xhr, ajaxOptions, thrownError) {
                console.log(arguments);
            });
    });
});

$.region_search = function () {
    var ul = $(".dropdown-search-box");
    var input = ul.find("input");
    var li = ul.find("li.result");

    input.keyup(function () {
        var val = $(this).val();

        if (val.length > 1) {
            li.hide();
            li.filter(':contains("' + val + '")').show();
        } else {
            li.show();
        }
    });

    $.expr[":"].contains = $.expr.createPseudo(function (arg) {
        return function (elem) {
            return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
        };
    });

    $(".dropdown-menu li span").click(function () {
        $(this).parents(".dropdown").find("#region").html($(this).text());
        $(this).parents(".dropdown").find("#region").attr("data-id",($(this).data('id')));
    });
};

$("#region").one("click", function (e) {
    $.ajax({
        url: "/ajax/region/",
        type: "GET",
        dataType: "html",
        beforeSend: function (xhr, settings) {
            $("#region").html("Загрузка...");            
        },
    })
        .done(function (result) {
            $("#region").html("Москва");
            $(".dropdown-menu").append(result);
            $.region_search();
        })
        .fail(function (xhr, ajaxOptions, thrownError) {
            console.log(arguments);
        });
});
