from django.urls import path
from . import views

app_name = 'competitors'
urlpatterns = [
    path('', views.index, name='index'),
    path('result', views.index, name='result'),
    path('ajax/form/', views.tool_form, name='form'),
 ]