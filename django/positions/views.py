import json
from django.shortcuts import render
from django.http import HttpResponse
from yandex.services import xml_yandex
from yandex.models import geo_db, serp_depth_db, xml_account_db

def tool_form(request):

    if request.method == "POST":

        if request.is_ajax():

            data = json.loads(request.body)
            website = data['website'][0]['value']
            top = data['top'][0]['value']
            region = data['region'][0]['value']
            user = xml_account_db['user']
            key = xml_account_db['key']

            queries = data['queries'][0]['value'].replace('\n',', ').split(',')

            context = {}
        
            number = 0
            result = ''
           
            top_3 = []
            top_4_10 = []
            top_11_20 = []
            top_21_30 = []
            top_30_plus = []
            top_100_plus = []

            for query in queries:

                try:

                    data = xml_yandex(user, key, query, region, top)
                    error = data[0]
                    domains = data[1]
                    urls = data[2]
                    

                    if error:
                        context = { 'error' : error.group(0), }
                        return render(request, 'yandex/error.html', context)

                    else:

                        if website in domains:
                            position = domains.index(website) + 1
                            url = urls[position - 1]

                        else:
                            position = 999
                            url = ''

                        if position <= 3: top_3.append(url)
                        if position >= 4 and position <= 10: top_4_10.append(url)
                        if position > 10 and position <= 20: top_11_20.append(url)
                        if position > 20 and position <= 30: top_21_30.append(url)
                        if position > 30 and position != 999: top_30_plus.append(url)
                        if position == 999: top_100_plus.append(url)

                        if position == 999:
                            position = f'{top}+'


                        result += f'<tr><th scope="row"></th><td>{query}</td><td>{position}</td><td><a target="_blank" href="{url}">{url}</a></td></tr>'
                        context = {
                        'result' : result,
                        'top_3': len(top_3),
                        'top_4_10': len(top_4_10),
                        'top_11_20': len(top_11_20),
                        'top_21_30': len(top_21_30),
                        'top_30_plus': len(top_30_plus),
                        'top_100_plus': len(top_100_plus),
                        'total_queries': len(queries),
                        'top' : top,
                        }

                except requests.ConnectionError : 
                    context = { 'error' : 'Сервер Yandex XML не отвечает!'}
                    return render(request, 'yandex/error.html', context)


        return render(request, 'positions/result.html', context)

    else :

        return HttpResponse('This is not Ajax')


def index(request):
    serp_depth = ''
    serp = ''

    for i in serp_depth_db: serp_depth += f'<option value="{i}">{serp_depth_db[i]}</option>'
    context = { 'serp_depth': serp_depth,}
    return render(request, 'positions/index.html', context )
        
