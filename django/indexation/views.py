import re
import json
import requests
from django.shortcuts import render
from yandex.services import xml_yandex
from yandex.models import geo_db, serp_depth_db
from yandex.models import geo_db, xml_account_db, serp_depth_db




def tool_form(request):

    if request.method == "POST":

        if request.is_ajax():

            data = json.loads(request.body)

 
            queries = data['queries'][0]['value'].replace('\n',', ').split(',')

 
            user = xml_account_db['user']
            key = xml_account_db['key']

            result = ''

            context = {}

            indexed = []
            not_in_the_index =[]

            error_exclude = ''
            error = ''

            for query in queries:

                query = f'url:{query.strip()}'
   
                try:
                    
                    data = xml_yandex(user, key, query, region='', top='')
                    modtime = data[4]
                    saved_copy_url = data[5]

                    if saved_copy_url != None:
                        copy_url = f'<a rel="nofollow" target=_blank href="{saved_copy_url.group(1)}">посмотреть</a>'
                    else:
                        copy_url = '-'

                    if modtime != None :
                        
                        day = modtime.group(3)
                        month = modtime.group(2)
                        year = modtime.group(1)
                        indexing_date = f'{day}-{month}-{year}'
                        index = '+'
                        error_exclude = None
                        error = None

                    else: 
                        index = '-'
                        indexing_date = '-'
                        error = data[0]
                        error_exclude = error.group(0)

                    if error and error_exclude != '<error code="15">Искомая комбинация слов нигде не встречается</error>' :
                        context = { 'error' : error.group(0), }
                        return render(request, 'yandex/error.html', context)

                    else:
                        if modtime != None: indexed.append(index)
                        if modtime == None: not_in_the_index.append(index)

                        query = query.replace('url:','')
                        result += f'<tr><th scope="row"></th><td>{query}</td><td>{index}</td><td>{indexing_date}</td><td>{copy_url}</td></tr>'
                        context = {
                            'result' : result,
                            'total_queries': len(queries),
                            'indexed' : len(indexed),
                            'not_in_the_index': len(not_in_the_index),
                        }

                except requests.ConnectionError : 
                    context = { 'error' : 'Сервер Yandex XML не отвечает!'}
                    return render(request, 'yandex/error.html', context)


        return render(request, 'indexation/result.html', context)

    else :

        return HttpResponse('This is not Ajax')

    
def index(request): 

    serp = ''
    region = ''
    serp_depth = ''
    
    for i in serp_depth_db: serp_depth += f'<option value="{i}">{serp_depth_db[i]}</option>'
    for i in geo_db: region += f'<option value="{i}">{geo_db[i]}</option>'
    context = { 'region': region, 'serp_depth': serp_depth, 'serp' : serp}
    return render(request, 'indexation/index.html', context )
        
