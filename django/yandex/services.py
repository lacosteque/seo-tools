from yandex.models import geo_db
from django.http import HttpResponse

import requests
import re
import time

def region_yandex(request):
    if request.is_ajax():
        region = ''
        for geo in geo_db: region += f'<li role="presentation" class="result"><span class="dropdown-item" data-id="{geo_db.get(geo)}" role="menuitem">{geo}</span></li>'
        context = region
        return HttpResponse(context)
    else :
        return HttpResponse('This is not Ajax')


def xml_yandex(user, key, query, region, top):
	time.sleep(1)
	xml_serps = requests.get(f'https://yandex.ru/search/xml?user={user}&key={key}&query={query}&lr={region}&l10n=ru&sortby=rlv&filter=none&maxpassages=1&groupby=attr%3D%22%22.mode%3Dflat.groups-on-page%3D{top}.docs-in-group%3D1&page=0').text
	error = re.search(r'<error code="(\d{0,9})">(.+)</error>',xml_serps)
	domains = re.findall(r'<domain>(.*?)</domain>', xml_serps)
	urls = re.findall(r'<url>(.*?)</url>', xml_serps)
	all_hlwords = re.findall(r'<hlword>(.+?)</hlword>', xml_serps)
	modtime = re.search(r'<modtime>(\d{0,4})(\d{0,2})(\d{0,2})T\d{0,9}</modtime>',xml_serps)
	saved_copy_url = re.search(r'<saved-copy-url>(.+?)</saved-copy-url>',xml_serps)

	return error, domains, urls, all_hlwords, modtime, saved_copy_url


	