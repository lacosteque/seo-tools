import json
from django.shortcuts import render
from django.http import HttpResponse
from yandex.services import xml_yandex
from yandex.models import geo_db, xml_account_db, serp_depth_db

def tool_form(request):

    if request.method == "POST":

        if request.is_ajax():

            data = json.loads(request.body)

            website = data['website'][0]['value']
            top = data['top'][0]['value']
            region = data['region'][0]['value']
            user = xml_account_db['user']
            key = xml_account_db['key']

            queries = data['queries'][0]['value'].replace('\n',', ').split(',')

            context = {}
        
            number = 0
            result = ''
   
            for query in queries:

                try:

                    data = xml_yandex(user, key, query, region, top)
                    error = data[0]
                    domains = data[1]
                    urls = data[2]
                    

                    if error:
                        context = { 'error' : error.group(0), }
                        return render(request, 'yandex/error.html', context)

                    else:

                        if website in domains:
                            position = domains.index(website) + 1
                            url = urls[position - 1]

                        else:
                            position = 999
                            url = ''


                        result += f'<tr><th scope="row"></th><td>{query}</td><td>{position}</td><td><a target="_blank" href="{url}">{url}</a></td></tr>'
                        context = {
                        'result' : result,
                        'total_queries': len(queries),
                        'top' : top,
                        }

                except requests.ConnectionError : 
                    context = { 'error' : 'Сервер Yandex XML не отвечает!'}
                    return render(request, 'yandex/error.html', context)


        return render(request, 'relevant/result.html', context)

    else :

        return HttpResponse('This is not Ajax')


def index(request):
    serp_depth = ''
    serp = ''

    
    for i in serp_depth_db: serp_depth += f'<option value="{i}">{serp_depth_db[i]}</option>'
    context = { 'serp_depth': serp_depth, 'serp': serp, }
    return render(request, 'relevant/index.html', context )
        
