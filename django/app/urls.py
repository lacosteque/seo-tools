from django.contrib import admin
from django.urls import include, path
from yandex.services import region_yandex
from django.views.generic.base import TemplateView


urlpatterns = [
    path('', include('homepage.urls'), name='home'),
    path('tools/password/', include('password.urls'), name='password'),
    path('tools/positions/', include('positions.urls'), name='positions'),
    path('tools/hlword/', include('hlword.urls'), name='hlword'),
    path('tools/age/', include('age.urls'), name='age'),
    path('tools/indexation/', include('indexation.urls'), name='indexation'),
    path('tools/competitors/', include('competitors.urls'), name='competitors'),
    path('tools/relevant/', include('relevant.urls'), name='relevant'),
    path('ajax/region/', region_yandex, name='region_yandex'),   
    path('robots.txt', TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),
    ),
]
