from django.apps import AppConfig


class HlwordConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hlword'
