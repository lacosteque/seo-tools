import json
from django.shortcuts import render
from django.http import HttpResponse
from yandex.services import xml_yandex
from yandex.models import geo_db, xml_account_db, serp_depth_db




def tool_form(request):

    if request.method == "POST":

        if request.is_ajax():
            data = json.loads(request.body)
            top = data['top'][0]['value']
            region = data['region'][0]['value']
            queries = data['queries'][0]['value'].replace('\n',', ').split(',')
            user = xml_account_db['user']
            key = xml_account_db['key']
            number = 0
            result = ''

            context = {}
            
            for query in queries:
                number = number + 1

                try:

                    data = xml_yandex(user, key, query, region, top)
                    error = data[0]
                    all_hlwords = data[3]

                    if error:
                        context = { 'error' : error.group(0), }
                        return render(request, 'yandex/error.html', context)

                    else:

                        hlwords = ', '.join(set([word.lower() for word in all_hlwords]))
                        result += f'<tr><th scope="row"></th><td>{query}</td><td>{hlwords}</td></tr>'
                        context = {
                            'result' : result,
                            'total_queries': len(queries),
                        }

                except requests.ConnectionError : 
                    context = { 'error' : 'Сервер Yandex XML не отвечает!'}
                    return render(request, 'yandex/error.html', context)


        return render(request, 'hlword/result.html', context)

    else :

        return HttpResponse('This is not Ajax')

    
def index(request): 

    serp = ''
    serp_depth = ''
    
    for i in serp_depth_db: serp_depth += f'<option value="{i}">{serp_depth_db[i]}</option>'
    context = { 'serp_depth': serp_depth, 'serp' : serp}
    return render(request, 'hlword/index.html', context )
        
