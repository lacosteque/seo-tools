from django.shortcuts import render
import random 
import string
import json

def is_ajax(request):
    return request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'

def index(request):

    if request.method == "POST":

        #if request.is_ajax():
        if is_ajax(request=request):
        
            context = {}
            password = ''
            digits = ''
            uppercase = ''
            punctuation = ''
            z = ''

            data = json.loads(request.body)

            input_digits = data['digits']
            input_uppercase = data['uppercase']
            input_punctuation = data['punctuation']
            lenght = int(data['lenght'])
            quantity = int(data['quantity'])

            if input_digits:
                digits = string.digits
            if input_uppercase:
                uppercase = string.ascii_uppercase
            if input_punctuation:
                punctuation = string.punctuation

            for x in range(quantity):

                characters = string.ascii_lowercase + digits + uppercase + punctuation
                password_value = (random.choice(characters) for i in range(lenght))

                password += f'<div onclick="copytext(\'#password_{x}\')" id="password_{x}" class="password h2 wow fadeIn" >{"".join(password_value)} <svg class ="ml-2" height="2rem" viewBox="-40 0 512 512" width="2rem" xmlns="http://www.w3.org/2000/svg"><path d="m271 512h-191c-44.113281 0-80-35.886719-80-80v-271c0-44.113281 35.886719-80 80-80h191c44.113281 0 80 35.886719 80 80v271c0 44.113281-35.886719 80-80 80zm-191-391c-22.054688 0-40 17.945312-40 40v271c0 22.054688 17.945312 40 40 40h191c22.054688 0 40-17.945312 40-40v-271c0-22.054688-17.945312-40-40-40zm351 261v-302c0-44.113281-35.886719-80-80-80h-222c-11.046875 0-20 8.953125-20 20s8.953125 20 20 20h222c22.054688 0 40 17.945312 40 40v302c0 11.046875 8.953125 20 20 20s20-8.953125 20-20zm0 0"/></svg></div>'
            context = {'password' : password,}

            return render(request, 'password/result.html', context )     

    else:

        return render(request, 'password/index.html' ) 
