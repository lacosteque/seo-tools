from django.urls import path
from . import views


app_name = 'age'
urlpatterns = [
    path('', views.index, name='index'),
    path('result', views.index, name='result'),
    path('form/', views.tool_form, name='form'),
    ]